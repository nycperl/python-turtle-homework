import turtle, random

"""
description:
bouncing balls with gravity simulation

"""

BORDER_Y = 230
BORDER_X = 300

class Ball(turtle.Turtle):
    def __init__(self, color):
        super().__init__()
        self.shape("circle")
        self.color(color)
        self.penup()
        self.speed(0)
        self.goto(0, 0)
        self.dy = random.randint(-2, 2)
        self.dx = random.randint(-3, 3)
        self.gravity = random.uniform(-.9, .9)
        print("color=", color, "gravity=", self.gravity)


wn = turtle.Screen()
wn.setup(700,500)
wn.bgcolor("black")
#Create the ball
b1 = Ball('red')
b2 = Ball('green')
b3 = Ball('yellow')
b4 = Ball('blue')
b5 = Ball('white')
balls_list = [b1, b2, b3, b4, b5]

# game loop
while True:
    #wn.update()
    for b in balls_list:
        wn.update()
        b.dy -= b.gravity
        b.sety(b.ycor() + b.dy)
        b.setx(b.xcor() + b.dx)
        #print (b.ycor())
        #Bounce
        if b.ycor() <= -1*BORDER_Y:
            b.sety(-1*BORDER_Y)
            b.dy *= -1
        if b.ycor() > BORDER_Y:
            b.sety(BORDER_Y)
            b.dy *= -1

        # right and left border checking
        if b.xcor() > BORDER_X:
            b.dx *= -1
        if b.xcor() < -1*BORDER_X:
            b.dx *= -1

wn.mainloop()