import turtle, random, time

"""
description:
turtle racing game
"""


screen = turtle.Screen()
WIDTH , HEIGHT, BG = 600, 600, "white"
screen.screensize(WIDTH, HEIGHT, BG)
screen.title("my first turtle screen")

# create a drawing pen
pen = turtle.Turtle()
pen.shape("square")
pen.color("red")
pen.penup()
pen.goto(230, 230)

def draw_one_border(heading=0):
    pen.setheading(heading)
    for i in range(13):
        pen.forward(35)
        pen.stamp()

border_dir = 270
for _ in range(4):
    draw_one_border(border_dir)
    border_dir -= 90


# Create 4 turtle and line them up on the left side
def create_turtle(t, color, heading):
    t.shape("turtle")
    t.color(color)

    t.penup()
    t.pendown()
    t.setheading(heading)

t1 = turtle.Turtle()
create_turtle(t1, 'red', 270)
t2 = turtle.Turtle()
create_turtle(t2, 'green', 0)
t3 = turtle.Turtle()
create_turtle(t3, 'blue', 90)
t4 = turtle.Turtle()
create_turtle(t4, 'black', 180)

turtle_list = [t1,t2,t3,t4]

def claim_winner(t):
    t1.write("WINNER!!", move = False, align = "right", font = ("Arial", 14, "bold"))
    t1.color("gold")

while True:
    for t in turtle_list:
        t.forward(random.randint(1, 5))

    if t1.ycor() < -230: #and t1.xcor() > t3.xcor() and t1.xcor() > t2.xcor() :
        claim_winner(t1)
        break
    if t2.xcor() > 230: # and t2.xcor() > t3.xcor() and t2.xcor() > t1.xcor():
        claim_winner(t2)
        break
    if t3.ycor() > 230: # and t3.ycor() > t2.ycor() and t3.ycor() > t1.ycor():
        claim_winner(t3)
        break
    if t4.ycor() > 230: # and t3.ycor() > t2.ycor() and t3.ycor() > t1.ycor():
        claim_winner(t4)
        break
screen.exitonclick()