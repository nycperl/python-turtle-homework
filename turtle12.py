import turtle, random, math

"""
description:
shooting bullets against two changing targets
"""

#Constants; global speeds
speed1 = 4
score = 0
style = ()
WIDTH, HEIGHT = 600, 600

# screen
screen = turtle.Screen()
screen.bgcolor("white")
screen.setup(WIDTH,HEIGHT)

colors = ["blue", "red", "green"]
turtles = []
for i in range(3):
    t = turtle.Turtle()
    t.color(colors[i])
    if i == 0:
        t.shape("turtle")
    else:
        t.shape("triangle")
    t.penup()
    t.speed(0)
    t.dx = random.randint(-1,4)
    t.dy = random.randint(-3,1)
    t.da = random.randint(1, 1)
    turtles.append(t)


# pen
pen = turtle.Pen()
pen.color("black")
pen.speed(0)
pen.penup()
pen.goto(-250,250)
pen.pendown()
for i in range(4):
    pen.fd(480)
    pen.rt(90)
pen.penup()
pen.setposition(0, 265)
pen.hideturtle()
pen.write("Score")


# ******************
# Creat the player's bullet
bullet = turtle.Turtle()
bullet.color("black")
bullet.shape("triangle")
bullet.penup()
bullet.speed(0)
bullet.setheading(90)
bullet.shapesize(0.5,0.5)
bullet.hideturtle()

bulletspeed = 30

# define bullet state
# ready - ready to fire
# fire - bullet is firing
bulletstate = "ready"

def fire_bullet():
    # Declare bulletstate as a global if it needs changed
    global bulletstate
    if bulletstate == "ready":
        bulletstate = "fire"
        # Move the bullet to the just above the player
        x = turtles[0].xcor()
        y = turtles[0].ycor()
        bullet.setposition(x,y)
        bullet.setheading(turtles[0].heading())
        bullet.speed(bulletspeed)
        bullet.showturtle()
# *******************


def turnleft():
    turtles[0].lt(15)

def turnright():
    turtles[0].rt(15)

def accelerate():
    global speed1
    speed1 += 1

def decelerate():
    global speed1
    speed1 -= 1

turtle.listen()
turtle.onkey(turnleft, "Left")
turtle.onkey(turnright, "Right")
turtle.onkey(accelerate, "Up")
turtle.onkey(decelerate, "Down")

turtle.onkey(fire_bullet, "space")

while True:
    screen.update()
    turtles[0].fd(speed1)

    for i in range(1,3):
        turtles[i].setx(turtles[i].xcor() + turtles[i].dx)
        turtles[i].sety(turtles[i].ycor() + turtles[i].dy)
        turtles[i].rt(15)

    if bulletstate == 'fire':
        bullet.showturtle()
        bullet.fd(bulletspeed)
        #bullet.setx(bullet.xcor() + speed2)

    # Border checkings
    for i in range(3):
        if turtles[i].xcor() >= 225 or turtles[i].xcor() <= -225:
            turtles[i].lt(turtles[i].da)
            turtles[i].dx *= -1
        if turtles[i].ycor() >= 225 or turtles[i].ycor() <= -225:
            turtles[i].rt(turtles[i].da)
            turtles[i].dy *= -1

    if bullet.xcor() >= 225 or bullet.xcor() <= -225 or bullet.ycor() >= 225 or bullet.ycor() <= -225:
        bullet.hideturtle()
        bullet.speed(0)
        bulletstate = 'ready'

    # collision
    if bulletstate == 'fire' and (math.sqrt(math.pow(bullet.xcor() - turtles[1].xcor(),2) + math.pow(bullet.ycor() - turtles[1].ycor(),2)) < 20 or math.sqrt(math.pow(bullet.xcor() - turtles[2].xcor(),2) + math.pow(bullet.ycor() - turtles[2].ycor(),2)) < 20):
        bullet.hideturtle()
        bulletstate = 'ready'
        bullet.speed(0)
        score += 10
        pen.clear()
        pen.write("score {} ".format(score), align = "center", font=("Verdana", 18, "normal"))
turtle.mainloop()