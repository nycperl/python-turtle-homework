import random
import time
import turtle

BATTLE_FIELD_SIDE = 200
PLAYER_MOVE_STEP = 10
BULLET_SPEED = 1
BULLET_STEP = 20
BULLET_HIT_DIST = 20
INVADER_DOWN_STEP = 5
SCORE_UNIT = 10

score = 0
invaders = []

class Invader(turtle.Turtle):
    def __init__(self, x, y):
        super().__init__()
        self.hideturtle()
        self.shape("turtle")
        self.color('red') 
        self.setheading(270)
        self.penup()
        self.goto(x, y)
        self.showturtle()

    def move_down(self):
        self.sety(self.ycor() - INVADER_DOWN_STEP)
        self.setx(self.xcor() + random.randrange(-5, 5))


class Player(turtle.Turtle):
    def __init__(self):
        super().__init__()
        self.hideturtle()
        self.shape("turtle")
        self.color('blue') 
        self.setheading(90)
        self.penup()
        self.goto(0, -1*BATTLE_FIELD_SIDE)
        self.showturtle()
        self.bullet = Bullet()

    def fire_bullet(self):
        if self.bullet.status == "ready":
            self.bullet.goto(self.xcor(), self.ycor())
            self.bullet.showturtle()
            self.bullet.status = 'fire'
        
    def move_bullet(self):
        if self.bullet.status == 'fire':
            self.bullet.fd(BULLET_STEP)

    def detect_crash(self):
        if not invaders:
            return True
        for i in invaders:
            if self.distance(i) < BULLET_HIT_DIST:
                self.color('red')
                print ("crashed!")
                return True
            if i.ycor() < -1*BATTLE_FIELD_SIDE - 40:
                invaders.remove(i)
        return False

class Bullet(turtle.Turtle):
    def __init__(self):
        super().__init__()
        self.hideturtle()
        self.shape("triangle")
        self.color('yellow') 
        self.penup()
        self.setheading(90)
        self.hideturtle()
        self.goto(0, -1*BATTLE_FIELD_SIDE)
        self.speed(BULLET_SPEED)
        self.status = 'ready'

    def update_status(self):
        if self.ycor() > BATTLE_FIELD_SIDE + 50 or self.detect_hit():
            self.hideturtle()
            self.status = 'ready'
        return

    def detect_hit(self):
        if self.status == 'ready':
            return False
        global score
        for i in invaders:
            if self.distance(i) < BULLET_HIT_DIST:
                i.hideturtle()
                invaders.remove(i)
                self.status = 'ready'
                print ('bullet hit!')
                score += SCORE_UNIT
                update_score()
                return True
        return False

        
            
def update_score(msg = None):
    if not msg:
        msg = "Score "+str(score)
    pen.clear()
    pen.write(msg, align = "left", font=("Verdana", 18, "normal"))


def move_left():
    if player.xcor()-PLAYER_MOVE_STEP <= -1*BATTLE_FIELD_SIDE:
        return
    player.setx(player.xcor()-PLAYER_MOVE_STEP)
    
def move_right():
    if player.xcor()+PLAYER_MOVE_STEP >= BATTLE_FIELD_SIDE:
        return
    player.setx(player.xcor()+PLAYER_MOVE_STEP)

def fire():
    if player.bullet.status != 'ready':
        return
    player.fire_bullet()

screen = turtle.Screen()
screen.bgcolor("black")
screen.title("Shoot Invaders Game")


for row in range(1,3):
    for i in range(1, 6):
        invader = Invader(-1*BATTLE_FIELD_SIDE + i * 60, BATTLE_FIELD_SIDE + row * 40)
        invaders.append(invader)


pen = turtle.Turtle()
pen.hideturtle()
pen.color("white")
pen.penup()
pen.goto(-300, 300)
update_score()

player = Player()
screen.listen()
screen.onkey(move_left, 'Left')
screen.onkey(move_right, 'Right')
screen.onkey(fire, 'space')

while True:
    # detect if player crashed invader
    if player.detect_crash():
        if not invaders:
            update_score("You Won!")
        else:
            update_score("You Got Hit! Game Over!")
        break
    # detect bullet state
    player.bullet.update_status()
    for i in invaders:
        i.move_down()
    if player.bullet.status == 'fire':
        player.move_bullet()

screen.mainloop()
