import turtle, random, math

"""
description:
shoot the bullets against the two changing targets
"""


#Constants; global speeds
speed1 = 4
score = 0
style = ()
WIDTH, HEIGHT = 600, 600

# screen
screen = turtle.Screen()
screen.bgcolor("white")
screen.setup(WIDTH,HEIGHT)

# turtle
t1 = turtle.Turtle()
t1.color("blue")
t1.shape("turtle")
t1.penup()
t1.speed(0)
t1.dx = random.randint(-3,3)
t1.dy = random.randint(-3,3)
t1.da = random.randint(5, 15)

t2 = turtle.Turtle()
t2.color("red")
t2.shape("triangle")
t2.penup()
t2.speed(0)
t2.dx = random.randint(-1,4)
t2.dy = random.randint(-3,1)
t2.da = random.randint(-1, 1)

t3 = turtle.Turtle()
t3.color("green")
t3.shape("triangle")
t3.penup()
t3.speed(0)
t3.dx = random.randint(-1,4)
t3.dy = random.randint(-3,1)
t3.da = random.randint(-1, 1)

# pen
pen = turtle.Pen()
pen.color("black")
pen.speed(0)
pen.penup()
pen.goto(-250,250)
pen.pendown()
for i in range(4):
    pen.fd(480)
    pen.rt(90)
pen.penup()
pen.setposition(0, 265)
pen.hideturtle()
pen.write("Score")


# ******************
# Creat the player's bullet
bullet = turtle.Turtle()
bullet.color("black")
bullet.shape("triangle")
bullet.penup()
bullet.speed(0)
bullet.setheading(90)
bullet.shapesize(0.5,0.5)
bullet.hideturtle()

bulletspeed = 30

# define bullet state
# ready - ready to fire
# fire - bullet is firing
bulletstate = "ready"

def fire_bullet():
    # Declare bulletstate as a global if it needs changed
    global bulletstate
    if bulletstate == "ready":
        bulletstate = "fire"
        # Move the bullet to the just above the player
        x = t1.xcor()
        y = t1.ycor()
        bullet.setposition(x,y)
        bullet.setheading(t1.heading())
        bullet.speed(bulletspeed)
        bullet.showturtle()
# *******************


def turnleft():
    t1.lt(15)

def turnright():
    t1.rt(15)

def accelerate():
    global speed1
    speed1 += 1

def decelerate():
    global speed1
    speed1 -= 1

turtle.listen()
turtle.onkey(turnleft, "Left")
turtle.onkey(turnright, "Right")
turtle.onkey(accelerate, "Up")
turtle.onkey(decelerate, "Down")

turtle.onkey(fire_bullet, "space")

while True:
    screen.update()
    t1.fd(speed1)

    t2.setx(t2.xcor() + t2.dx)
    t2.sety(t2.ycor() + t2.dy)
    t2.rt(15)

    t3.setx(t3.xcor() + t3.dx)
    t3.sety(t3.ycor() + t3.dy)
    t3.rt(15)
    
    if bulletstate == 'fire':
        bullet.showturtle()
        bullet.fd(bulletspeed)
        #bullet.setx(bullet.xcor() + speed2)

    # Border checkings
    if t1.xcor() >= 225 or t1.xcor() <= -225:
        t1.lt(t1.da)
        t1.dx *= -1
    if t1.ycor() >= 225 or t1.ycor() <= -225:
        t1.rt(t1.da)
        t1.dy *= -1

    if t2.xcor() >= 225 or t2.xcor() <= -225:
        t2.lt(t2.da)
        t2.dx *= -1
    if t2.ycor() >= 225 or t2.ycor() <= -225:
        t2.rt(t2.da)
        t2.dy *= -1

    if t3.xcor() >= 225 or t3.xcor() <= -225:
        t3.lt(t3.da)
        t3.dx *= -1
    if t3.ycor() >= 225 or t3.ycor() <= -225:
        t3.rt(t3.da)
        t3.dy *= -1

    if bullet.xcor() >= 225 or bullet.xcor() <= -225 or bullet.ycor() >= 225 or bullet.ycor() <= -225:
        bullet.hideturtle()
        bullet.speed(0)
        bulletstate = 'ready'

    # collision
    if bulletstate == 'fire' and (math.sqrt(math.pow(bullet.xcor() - t2.xcor(),2) + math.pow(bullet.ycor() - t2.ycor(),2)) < 20 or math.sqrt(math.pow(bullet.xcor() - t3.xcor(),2) + math.pow(bullet.ycor() - t3.ycor(),2)) < 20):
        bullet.hideturtle()
        bulletstate = 'ready'
        bullet.speed(0)
        score += 10
        pen.clear()
        pen.write("score {} ".format(score), align = "center", font=("Verdana", 18, "normal"))
turtle.mainloop()