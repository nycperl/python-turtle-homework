import turtle, random, math

"""
description:
chase after the changing target
"""


#Constants
speed = 1
WIDTH, HEIGHT = 700, 700
screen = turtle.Screen()
screen.bgcolor("lightblue")
screen.setup(WIDTH,HEIGHT)

hit_distance_buffer = 15
count = 0
score_unit = 10

t1 = turtle.Turtle()
t1.color("blue")
t1.shape("triangle")
t1.penup()
t1.speed(0)

def place_ball(b):
    x = random.randrange(WIDTH/2)
    y = random.randrange(HEIGHT/2)
    b.goto(x, y)

ball = turtle.Turtle()
ball.color("red")
ball.shape("circle")
ball.penup()
ball.speed(0)
place_ball(ball)

scoreboard = turtle.Turtle()
scoreboard.penup()
scoreboard.hideturtle()
scoreboard.goto(-200, 200)

def update_scoreboard(c):
    scoreboard.clear()
    scoreboard.write("Score: " + str(c * score_unit), move = False, align = "right", font = ("Arial", 14, "bold"))

def turnleft():
    t1.left(30)

def turnright():
    t1.rt(30)

def accelerate():
    global speed
    speed += 1

turtle.listen()
turtle.onkey(turnleft, "Left")
turtle.onkey(turnright, "Right")
turtle.onkey(accelerate, "Up")

update_scoreboard(0)


def detect_collide(t, b):
    distance = math.sqrt( (t.xcor() - b.xcor())**2 + (t.ycor() - b.ycor())**2 )
    if ( distance < hit_distance_buffer ):
        global count 
        count += 1
        
        update_scoreboard(count)
        # move the target to somewhere else
        place_ball(b)

def reverse_sign(x):
    return abs(x)/x*(-1)

while True:
    screen.update()
    detect_collide(t1, ball)
    t1.fd(speed)
    if abs(t1.xcor()) >= WIDTH/2:
        t1.setx( reverse_sign(t1.xcor()) * WIDTH/2) 
    if abs(t1.ycor()) >= HEIGHT/2:
        t1.sety( reverse_sign(t1.ycor()) * HEIGHT/2)    

turtle.mainloop()
