import math

x1 = float(input('enter x1: '))
y1 = float(input('enter y1: '))
x2 = float(input('enter x2: '))
y2 = float(input('enter y2: '))

dist = math.sqrt(pow((x2-x1),2) + pow((y2-y1),2))

print('the distance of those two points is: ', dist)

slope = (y2-y1)/(x2-x1)
print('the slope of those two points is: ', slope)