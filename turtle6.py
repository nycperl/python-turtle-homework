import turtle, math

"""
description:
coop shooting game
"""


PLAYGROUND_SIDE =200
INVINCIBLE_BUFFER = 25

class Turtle(turtle.Turtle):
    step = 10
    icon = 'turtle'

    def __init__(self, color, side):
        super().__init__()
        self.shape(Turtle.icon)
        self.color(color) 
        self.penup()
        self.name = color
        self.side = side
        self.hits = 0
        self.invincible = 0
        self.flying_bullets = []

        if (side == 'left'):
            self.goto((-1) * PLAYGROUND_SIDE, 0)
            self.setheading(0)
        elif (side == 'right'):
            self.goto(PLAYGROUND_SIDE, 0)
            self.setheading(180)

    def up(self):
        if (self.ycor() < PLAYGROUND_SIDE):
            self.sety(self.ycor() + Turtle.step)

    def down(self):
        if (self.ycor() > (-1)*PLAYGROUND_SIDE):
            self.sety(self.ycor() - Turtle.step)

    def fire_bullet(self):
        bullet = Bullet(self.side, self.name)
        bullet.goto(self.xcor(), self.ycor())
        if self.side == 'right':
            bullet.setheading(180)
        elif self.side == 'left':
            bullet.setheading(0)
        bullet.showturtle()
        self.flying_bullets.append(bullet)
        bullet.speed(1)


class Bullet(turtle.Turtle):
    step = 10
    icon = 'arrow'

    def __init__(self, side, color):
        super().__init__(visible=False)
        self.shape(Bullet.icon)
        self.color(color)
        self.penup()
        self.side = side
        self.alive = True

    def move(self):
        if (self.side == 'left'):
            if self.xcor() < PLAYGROUND_SIDE + 30:
                self.fd(Bullet.step)
            else:
                self.alive = False
        elif (self.side == 'right'):
            if self.xcor() > (-1)*PLAYGROUND_SIDE - 30:
                self.fd(Bullet.step)
            else:
                self.alive = False


class ScoreBoard(turtle.Turtle):
    def __init__(self, side):
        super().__init__()
        self.penup()
        self.hideturtle()
        if side == 'right':
            self.goto(PLAYGROUND_SIDE, PLAYGROUND_SIDE+20)
        elif side == 'left':
            self.goto((-1)*PLAYGROUND_SIDE, PLAYGROUND_SIDE+20)
        self.write("Goals: 0", move = False, align = "right", font = ("Arial", 14, "bold"))

    def update_score(self, score_text):
        self.clear()
        self.write("Goals: " + str(score_text), move = False, align = "right", font = ("Arial", 14, "bold"))


class Game():
    def __init__(self, t1, t2, s1, s2):
        self.t1 = t1
        self.t2 = t2
        self.s1 = s1 
        self.s2 = s2

    def move_turtle(self):
        screen1 = self.t1.getscreen()
        screen1.listen()

        screen1.onkey(self.t1.up, "Up")
        screen1.onkey(self.t1.down, "Down")
        screen1.onkey(self.t1.fire_bullet, "space")
        
        screen2 = self.t2.getscreen()
        screen2.onkey(self.t2.up, "a")
        screen2.onkey(self.t2.down, "z")
        screen1.onkey(self.t2.fire_bullet, "x")

    def play(self):
        self.move_turtle()
        while True:
            # this is a hidden bullet to avoid while loop stuck
            hidden_bullet = Bullet('right', 'black')
            hidden_bullet.hideturtle()
            hidden_bullet.move()

            for t in (self.t1, self.t2):
                if t.invincible:
                    t.invincible -= 1
                if not t.invincible:
                    t.color(t.name)

            for bullet in (self.t1.flying_bullets + self.t2.flying_bullets):
                if bullet.alive:
                    bullet.move()
                    self.detect_hit(bullet)
                else:
                    # hide bullet from the screen
                    bullet.hideturtle()
                    # remove the 'hit' bullet from the bullets array
                    if bullet in self.t1.flying_bullets:
                        self.t1.flying_bullets.remove(bullet)
                    if bullet in self.t2.flying_bullets:
                        self.t2.flying_bullets.remove(bullet)
                    # destroy the 'hit' bullet object to save memory
                    del bullet

            del hidden_bullet

    def detect_hit(self, bullet):
        hit_distance_buffer = 12
        turtles = [self.t1, self.t2]
        for i in (0, 1):
            t = turtles[i]
            # skip detecting hit by own bullets
            if t.color() == bullet.color():
                continue

            distance = math.sqrt( (t.xcor() - bullet.xcor())**2 + (t.ycor() - bullet.ycor())**2 )
            # the main collision detection condition logic
            if t.invincible == 0 and distance < hit_distance_buffer:
                t.hits += 1
                print("hit " + t.name) 
                t.color('gold')
                t.invincible = INVINCIBLE_BUFFER
                if i == 0:
                    self.s2.update_score(t.hits)
                else:
                    self.s1.update_score(t.hits)


def draw_playground():
    turtle.title("Hit the Turtle Game")
    turtle.color('black')
    turtle.shape('circle')
    turtle.penup()
    turtle.goto(PLAYGROUND_SIDE * (-1), PLAYGROUND_SIDE)
    turtle.pendown()
    for _ in range(4):
        turtle.forward(PLAYGROUND_SIDE * 2)
        turtle.stamp()
        turtle.right(90)

draw_playground()

t1 = Turtle('red', 'right')
t2 = Turtle('blue', 'left')
score_board1 = ScoreBoard('right')
score_board2 = ScoreBoard('left')

game = Game(t1, t2, score_board1, score_board2)
game.play()

turtle.mainloop() 
