import turtle
import random
import time

"""
description:
turtle racing game
"""

FINISH_LINE_LENGTH = 200

class Turtle(turtle.Turtle):
    stamp_on = 0
    step_max = 30
    icon = 'turtle'
    pause = 0.1

    def __init__(self, color):
        super().__init__()
        self.shape(Turtle.icon)
        self.color(color) 

    def get_ready(self, direction, x=0, y=0):
        self.penup()
        self.goto(x, y)
        dir = 0
        if (direction == 'east'):
            dir = 0
        elif (direction == 'north'):
            dir = 90
        elif (direction == 'west'):
            dir = 180
        elif (direction == 'south'):
            dir = 270

        self.setheading(dir)
        self.pendown()
    
    def move(self):
        if Turtle.stamp_on:
            self.stamp()
        step = random.randrange(1, Turtle.step_max)
        self.forward(step)
        return True

        #time.sleep(Turtle.pause)
    def is_winner(self):
        if (self.heading() == 0.0 and self.xcor() < FINISH_LINE_LENGTH):
            return False
        elif (self.heading() == 180.0 and self.xcor() > (-1)*FINISH_LINE_LENGTH):
            return False
        elif (self.heading() == 90.0 and (self.ycor() < FINISH_LINE_LENGTH)):
            return False
        elif (self.heading() == 270.0 and self.ycor() > (-1)*FINISH_LINE_LENGTH):
            return False
        else:
            self.color('gold')
            self.write("Winner!", move=False, align='center', font=['Arial', 50, 'normal'])
            return True

        

def draw_finish_lines():
    turtle.color('black')
    turtle.shape('circle')
    turtle.penup()
    # spare coord: (-200, 200), (200, 200), (200, -200), (-200, 200)
    turtle.goto(FINISH_LINE_LENGTH * (-1), FINISH_LINE_LENGTH)
    turtle.pendown()
    for _ in range(4):
        turtle.forward(FINISH_LINE_LENGTH * 2)
        turtle.stamp()
        turtle.right(90)

draw_finish_lines()


t1 = Turtle('red')
t1.get_ready('north')

t2 = Turtle('green')
t2.get_ready('east')

t3 = Turtle('blue')
t3.get_ready('south')

t4 = Turtle('purple')
t4.get_ready('west')

while True:
    if (t1.move() and t1.is_winner()):
        break
    if (t2.move() and t2.is_winner()):
        break 
    if (t3.move() and t3.is_winner()):
        break
    if (t4.move() and t4.is_winner()):
        break 

turtle.mainloop()