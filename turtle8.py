import turtle, random, time, math

"""
description:
towards the random position
"""

screen = turtle.Screen()

hit_distance_buffer = 15
count = 0
max_count = 5

target = turtle.Turtle()
target.speed(0)

follow = turtle.Turtle("turtle")
follow.speed(0)
follow.color("red")
follow.penup()
follow.setposition(-250, -250)

def mouseEvent1(x,y):
    print("screen was click at ",x,y)
    target.penup()
    target.goto(x,y)
    target.pendown()
    target.write(str(x) + " , " +str(y))

def detect_collide():
    distance = math.sqrt( (target.xcor() - follow.xcor())**2 + (target.ycor() - follow.ycor())**2 )
    if ( distance < hit_distance_buffer ):
        global count 
        count += 1
       
        # make the follow change color
        follow.color("gold")
        follow.write("I got you!", move = False, align = "right", font = ("Arial", 14, "bold"))
        # move the target to somewhere else
        mouseEvent1(random.randrange(-250, 250), random.randrange(-250, 250))
        # change color back
        follow.color("red")
        # clear everything after max count
        if (count >= max_count):
            follow.clear()
            target.clear()
            count = 0


def follow_runner():
    follow.setheading(follow.towards(target))
    follow.forward(1)
    detect_collide()
    turtle.ontimer(follow_runner, 0)

screen.onclick(mouseEvent1, 1)
screen.listen()

while True:
    screen.update() 
    follow_runner() 
screen.mainloop()
