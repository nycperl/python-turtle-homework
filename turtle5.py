from cmath import sqrt
import turtle, random, math

'''
description:
shooting game

features:
- two balls randomly move within the play area, reflect when hit the wall
- one turtle can move horizontally, controlled by the left/right arrow keys
- the turtle can fire bullets triggered by pressing the 'space' key
- a score board to track the 'hits' score
- when the bullet hits the ball, the ball will temporarily change color to 'gold'
- the balls have a short 'invincible' period when hit by bullet. hits are not counted during
invincible period. the ball will remain in 'gold' color when 'invincible', but will change to 
its original color when 'invincible period' expires
- the score board will keep the count of how many times the balls are hit
- print the 'hit log' in the command line - which color ball is hit 
'''


PLAYGROUND_SIDE =200
COLLISION_BUFFER = 12
INVINCIBLE_BUFFER = 25

class Ball(turtle.Turtle):
    step = 5
   
    def __init__(self, color):
        super().__init__()
        self.shape('circle')
        self.color(color)
        self.goto(0, 0)
        self.setheading(random.randrange(0, 360))
        self.penup()
        self.name = color
        self.invincible = 0

    def move(self):
        self.forward(Ball.step)
        # if hitting the border, then change dir (reflection)
        if self.xcor() >= PLAYGROUND_SIDE or self.xcor() <= (-1) * PLAYGROUND_SIDE:
            self.setheading( 180 - self.heading() )
        if self.ycor() >= PLAYGROUND_SIDE or self.ycor() <= (-1) * PLAYGROUND_SIDE:
            self.setheading( 360 - self.heading() )
        
        if self.invincible:
            self.invincible -= 1
            if not self.invincible:
                self.color(self.name)

class Turtle(turtle.Turtle):
    step = 10
    icon = 'turtle'

    def __init__(self, color):
        super().__init__()
        self.shape(Turtle.icon)
        self.color(color) 
        self.penup()
        self.goto(0, (-1) * PLAYGROUND_SIDE - 30)
        self.setheading(90)

    def left(self):
        if (self.xcor() > (-1) * PLAYGROUND_SIDE):
            self.setx(self.xcor() - Turtle.step)

    def right(self):
        if (self.xcor() < PLAYGROUND_SIDE):
            self.setx(self.xcor() + Turtle.step)


class Bullet(turtle.Turtle):
    step = 5
    icon = 'arrow'

    def __init__(self):
        super().__init__(visible=False)
        self.shape(Bullet.icon)
        self.color('black')
        self.penup()
        self.alive = True

    def move(self):
        if (self.ycor() < PLAYGROUND_SIDE + 30):
            self.sety( self.ycor() + Bullet.step)
        else:
            self.alive = False

class ScoreBoard(turtle.Turtle):
    def __init__(self):
        super().__init__()
        self.penup()
        self.hideturtle()
        self.goto(PLAYGROUND_SIDE, PLAYGROUND_SIDE+20)
        self.write("Hits: 0", move = False, align = "right", font = ("Arial", 14, "bold"))

    def update_score(self, score_text):
        self.clear()
        self.write("Hits: " + str(score_text), move = False, align = "right", font = ("Arial", 14, "bold"))

class Game():
    def __init__(self, b1, b2, turtle, score_board):
        self.b1 = b1
        self.b2 = b2
        self.turtle = turtle
        self.score_board = score_board
        self.bullets = []
        self.hits = 0

    def fire_bullet(self):
        bullet = Bullet()
        self.bullets.append(bullet)
        bullet.goto(self.turtle.xcor(), self.turtle.ycor())
        bullet.setheading(90)
        bullet.showturtle()

    def move_turtle(self):
        screen = self.turtle.getscreen()
        screen.listen()

        screen.onkey(self.turtle.left, "Left")
        screen.onkey(self.turtle.right, "Right")
        screen.onkey(self.fire_bullet, "space")

    def play(self):
        self.move_turtle()
        while True:
            for ball in (self.b1, self.b2):
                ball.move()
            
            for bullet in (self.bullets):
                if bullet.alive:
                    bullet.move()
                    self.detect_hit(bullet)
                else:
                    # hide bullet from the screen
                    bullet.hideturtle()
                    # remove the 'hit' bullet from the bullets array
                    self.bullets.remove(bullet)
                    # destroy the 'hit' bullet object to save memory
                    del bullet

    def detect_hit(self, bullet):
        hit_distance_buffer = 10
        for b in (self.b1, self.b2):
            distance = math.sqrt( (b.xcor() - bullet.xcor())**2 + (b.ycor() - bullet.ycor())**2 )
            # the main collision detection condition logic
            if b.invincible == 0 and distance < hit_distance_buffer:
                self.hits += 1
                self.score_board.update_score(self.hits)
                print("hit " + b.name) 
                b.color('gold')
                b.invincible = INVINCIBLE_BUFFER


def draw_playground():
    turtle.title("Hit the Balls Game")
    turtle.color('black')
    turtle.shape('circle')
    turtle.penup()
    turtle.goto(PLAYGROUND_SIDE * (-1), PLAYGROUND_SIDE)
    turtle.pendown()
    for _ in range(4):
        turtle.forward(PLAYGROUND_SIDE * 2)
        turtle.stamp()
        turtle.right(90)

draw_playground()

ball1 = Ball('red')
ball2 = Ball('blue')
turtle = Turtle('green')
score_board = ScoreBoard()

game = Game(ball1, ball2, turtle, score_board)
game.play()