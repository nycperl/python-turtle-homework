weight = int(input('enter your weight in pounds: '))
height = int(input('enter your height in inches: '))

bmi = round((weight * 703 / (height * height)), 1)

value = ''
if bmi < 18.5:
    value = 'underweight'
elif bmi >= 18.5 and bmi < 25:
    value = 'normal'
elif bmi >= 25 and bmi < 30:
    value = 'overweight'
else:
    value = 'obese'

print ('your BMI is: ', bmi)
print ('you are ', value)
