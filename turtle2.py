from cmath import sqrt
import turtle
import math

"""
description:
ping pong game
"""

HEIGHT = 400
LEFT_BORDER = -400
RIGHT_BORDER = 400

class Ball(turtle.Turtle):
    ball_color = 'red'
    step = 2
    initial_heading = 50

    def __init__(self):
        super().__init__()
        self.outside_border = False
        self.ending_ycor = None
        self.shape('circle')
        self.color(Ball.ball_color)
        self.goto(0, 0)
        #self.mode('standard')
        self.setheading(Ball.initial_heading)
        self.penup()
        

    def move(self):
        self.forward(Ball.step)

        buffer = 50
    
        # if hitting the left or right border, then change dir
        if self.xcor() >= RIGHT_BORDER or self.xcor() <= LEFT_BORDER:
            self.setheading( 180 - self.heading() )

        if self.ycor() >= HEIGHT // 2 + buffer or self.ycor() <= (-1) * HEIGHT // 2 - buffer:
            self.outside_border = True
            self.ending_ycor = self.ycor()


class Paddle(turtle.Turtle):
    step = 50
    poly = ((10,50),(10,-50),(-10,-50),(-10,50))
    color = 'green'

    def left_red(self):
        if (self.xcor() > LEFT_BORDER):
            self.backward(Paddle.step)

    def right_red(self):
        if (self.xcor() < RIGHT_BORDER):
            self.forward(Paddle.step)

    def left_blue(self):
        if (self.xcor() > LEFT_BORDER):
            self.backward(Paddle.step)

    def right_blue(self):
        if (self.xcor() < RIGHT_BORDER):
            self.forward(Paddle.step)


    def __init__(self, pos):
        super().__init__()
        self.pos = pos
        self.penup()

        if (pos == "north"):
            position = HEIGHT // 2
        elif (pos == "south"):
            position = HEIGHT//2*(-1) 
        self.goto(0, position)
        
        shape = turtle.Shape("compound")
        shape.addcomponent(Paddle.poly, Paddle.color)

        screen = turtle.Screen()
        screen.register_shape("paddle", shape)
        self.shape('paddle')
        

    def move(self):
        screen = self.getscreen()
        pos = self.pos

        screen.listen()
        if (pos == "north"):
            screen.onkey(self.left_red, "Left")
            screen.onkey(self.right_red, "Right")
        elif (pos == "south"):
            screen.onkey(self.left_blue, "z")
            screen.onkey(self.right_blue, "x")

class Game():
    def __init__(self, p1, p2, ball):
        self.p1 = p1 
        self.p2 = p2 
        self.ball = ball 
        

    def play(self):
        self.p1.move()
        self.p2.move()
        self.ball.move()

        while not self.ball.outside_border:
            self.ball.move()
            self.detect_hit(self.p1, self.p2, self.ball)

        if self.ball.ending_ycor > 0:
            self.write_winner(p2)
        else:
            self.write_winner(p1)

    def write_winner(self, p):
        p.write("WINNER!!", move = False, align = "right", font = ("Arial", 14, "bold"))

    def detect_hit(self, p1, p2, ball):
        if (abs(ball.xcor() - p1.xcor()) <= 50 and abs(ball.ycor() - p1.ycor()) <= 2) or (abs(ball.xcor() - p2.xcor()) <= 50 and abs(ball.ycor() - p2.ycor()) <= 2):
#        if math.sqrt( (ball.xcor() - p1.xcor())**2 + (ball.ycor() - p1.ycor())**2 ) < 35 or math.sqrt( (ball.xcor() - p2.xcor())**2 + (ball.ycor() - p2.ycor())**2 ) < 35:
            ball.setheading(360 - ball.heading())           


def draw_line():
    turtle.color('black')
    turtle.shape('circle')
    turtle.penup()
    turtle.goto(-400, 0)
    turtle.stamp()
    turtle.pendown()
    turtle.forward(800)
    turtle.stamp()
    
#print (turtle.Screen().screensize())
draw_line()

p1 = Paddle("north")
p2 = Paddle("south")
ball = Ball()

game = Game(p1, p2, ball)
game.play()

turtle.mainloop()  # This will make sure the program continues to run 