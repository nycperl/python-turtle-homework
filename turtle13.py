import turtle, random, math

"""
description:
shoot bullets against 3 changing targets

"""


#Constants; global speeds
# player's speed
speed1 = 4
rotate_degree = 15
#bullet's speed
speed2 = 30
score = 0
WIDTH, HEIGHT = 400, 400

# screen
screen = turtle.Screen()
screen.bgcolor("white")
screen.setup(WIDTH*2,HEIGHT*2)

# the player and 3 floating balls
colors = ["red", "blue", "green", "yellow"]
turtles = []
for i in range(4):
    t = turtle.Turtle()
    t.color(colors[i])
    if i: 
        t.shape("square")
    else:
        t.shape("turtle")
    t.penup()
    t.speed(0)
    t.dx = random.randint(-1,4)
    t.dy = random.randint(-3,1)
    t.da = random.randint(1, 1)
    turtles.append(t)

# pen to draw the borders 
pen = turtle.Pen()
pen.color("black")
pen.speed(0)
pen.penup()
pen.hideturtle()
pen.goto(-WIDTH/2,HEIGHT/2)
pen.pendown()
for i in range(4):
    pen.fd(WIDTH)
    pen.rt(90)

# write score
score_pen = turtle.Pen()
score_pen.penup()
score_pen.setposition(0, 265)
score_pen.hideturtle()
score_pen.write("Score 0", align = "center", font=("Verdana", 18, "normal"))


# Creat the player's bullet
bullet = turtle.Turtle()
bullet.color("black")
bullet.shape("triangle")
bullet.penup()
bullet.speed(0)
bullet.setheading(90)
bullet.shapesize(0.5,0.5)
bullet.hideturtle()

bulletspeed = speed2

# define bullet state
# ready - ready to fire
# fire - bullet is firing
bulletstate = "ready"

def fire_bullet():
    # Declare bulletstate as a global if it needs changed
    global bulletstate
    if bulletstate == "ready":
        bulletstate = "fire"
        # Move the bullet to the just above the player
        x = turtles[0].xcor()
        y = turtles[0].ycor()
        bullet.setposition(x,y)
        bullet.setheading(turtles[0].heading())
        bullet.speed(bulletspeed)
        bullet.showturtle()


def turnleft():
    turtles[0].lt(rotate_degree)

def turnright():
    turtles[0].rt(rotate_degree)

def accelerate():
    global speed1
    speed1 += 1

def decelerate():
    global speed1
    speed1 -= 1

# associate keys to functions
turtle.listen()
turtle.onkey(turnleft, "Left")
turtle.onkey(turnright, "Right")
turtle.onkey(accelerate, "Up")
turtle.onkey(decelerate, "Down")

turtle.onkey(fire_bullet, "space")

while True:
    screen.update()
    turtles[0].fd(speed1)

    for i in range(1,4):
        turtles[i].setx(turtles[i].xcor() + turtles[i].dx)
        turtles[i].sety(turtles[i].ycor() + turtles[i].dy)
        turtles[i].rt(rotate_degree)

    if bulletstate == 'fire':
        bullet.showturtle()
        bullet.fd(bulletspeed)
        #bullet.setx(bullet.xcor() + speed2)

    # Border checkings
    for i in range(4):
        if turtles[i].xcor() >= WIDTH/2 or turtles[i].xcor() <= -1*WIDTH/2:
            turtles[i].lt(rotate_degree)
            turtles[i].dx *= -1
        if turtles[i].ycor() >= HEIGHT/2 or turtles[i].ycor() <= -1*HEIGHT/2:
            turtles[i].rt(rotate_degree)
            turtles[i].dy *= -1

    if bullet.xcor() >= WIDTH/2 or bullet.xcor() <= -1*WIDTH/2 or bullet.ycor() >= HEIGHT/2 or bullet.ycor() <= -1*HEIGHT/2:
        bullet.hideturtle()
        bullet.speed(0)
        bulletstate = 'ready'

    # collision
    if bulletstate == 'fire':
        is_hit = False
        for i in range(1, 4):
            is_hit = is_hit or math.sqrt(math.pow(bullet.xcor() - turtles[i].xcor(),2) + math.pow(bullet.ycor() - turtles[i].ycor(),2)) < 20
        if is_hit:
            bullet.hideturtle()
            bulletstate = 'ready'
            bullet.speed(0)
            score += 10
            score_pen.clear()
            score_pen.write("score {} ".format(score), align = "center", font=("Verdana", 18, "normal"))
turtle.mainloop()