from distutils.debug import DEBUG
import turtle, random, time

"""
description:
frog cross road game
"""

PLAYGROUND_SIDE =400
FINISH_LINE = PLAYGROUND_SIDE - 200
DEBUG = 0

class Car(turtle.Turtle):

    def __init__(self, color, speed, step, length, starting_x, y, direction):
        super().__init__()
        self.hideturtle()
        self.shape("square")
        self.color(color) 
        self.penup()
        self.shapesize(stretch_wid=1, stretch_len=length)
        if (direction == 'right'):
            self.setheading(0)
        else:
            self.setheading(180)
        self.goto(starting_x, y)
        self.showturtle()
        self.step = step
        self.speed(speed)

    def move(self):
        self.fd(self.step)
        self.detect_hit()
        if self.xcor() > PLAYGROUND_SIDE or self.xcor() < PLAYGROUND_SIDE*(-1):
            self.hideturtle()
            self.setx(PLAYGROUND_SIDE * (-1) * abs(self.xcor())/self.xcor())
            self.showturtle()
            
    def detect_hit(self):
        if abs(self.ycor()-frog.ycor()) < 20 and abs(self.xcor()-frog.xcor()) < 40:
            frog.hits += 1
            if DEBUG: print ("hits", frog.hits)
            write_result('failed')
        if frog.ycor() > FINISH_LINE:
            write_result('won')
        

class Lane():
    lane_length = PLAYGROUND_SIDE*2
    space_between_car = 100
    lowest_lane_y = PLAYGROUND_SIDE*(-1) + 200
    lane_y_space = 60

    def __init__(self, lane_num, car_color, car_speed, car_step, max_car_length, direction):
        self.lane_num = lane_num
        self.lane_y = Lane.lowest_lane_y + lane_num * Lane.lane_y_space
        self.car_color = car_color
        self.car_speed = car_speed
        self.car_step = car_step
        self.max_car_length = max_car_length
        self.direction = direction
        self.cars = []

    def add_cars(self):
        unit_space = self.max_car_length + Lane.space_between_car
        num = Lane.lane_length // unit_space + 1
        if DEBUG: print ("unit space", unit_space)
        if DEBUG: print ("num", num)
        for i in range(num):
            car_x = PLAYGROUND_SIDE*(-1) + i * unit_space + random.randint(-10,30)
            if DEBUG: print ("car x", car_x)
            car = Car(self.car_color, self.car_speed, self.car_step, self.max_car_length, car_x, self.lane_y, self.direction)
            self.cars.append(car)

    def flow(self):
        if not self.cars: 
            return
        for car in self.cars:
            if DEBUG: print ("car x", car.xcor())
            car.move()

class Frog(turtle.Turtle):
    step = 5
    def __init__(self):
        super().__init__()
        self.hideturtle()
        self.shape("turtle")
        self.color('black') 
        self.penup()
        self.setheading(90)
        self.goto(0, Lane.lowest_lane_y-50)
        self.showturtle()
        self.speed(10)
        self.hits = 0
        
def move_left():
    frog.setx(frog.xcor()-Frog.step)

def move_right():
    frog.setx(frog.xcor()+Frog.step)

def move_up():
    frog.sety(frog.ycor()+Frog.step)
    #score_pen.clear()

screen = turtle.Screen()
screen.bgcolor("orange")
screen.title('Turtle Game: Cross the Street')
lanes = []
lane_color = {1:'red', 2:'yellow', 3:'blue', 4:'green' }
for i in range(1,5):
    lane = Lane(i, lane_color[i], random.randint(1,5), random.randint(10,20), random.randint(2,3), random.choice(['left', 'right']))
    lane.add_cars()
    lanes.append(lane)

def draw_curbs():
    pen = turtle.Pen()
    pen.color("black")
    pen.speed(0)
    pen.penup()
    pen.hideturtle()
    pen.goto(-PLAYGROUND_SIDE, Lane.lowest_lane_y-50)
    pen.pendown()
    pen.fd(PLAYGROUND_SIDE*2)
    pen.penup()
    pen.goto(-PLAYGROUND_SIDE, FINISH_LINE)
    pen.pendown()
    pen.fd(PLAYGROUND_SIDE*2)

draw_curbs()

frog = Frog()
screen.listen()
screen.onkey(move_left, 'Left')
screen.onkey(move_right, 'Right')
screen.onkey(move_up, 'Up')

score_pen = turtle.Pen()
score_pen.penup()
score_pen.setposition(0, 265)
score_pen.hideturtle()

def write_result(msg):
    restart = False
    if msg == 'failed':
        msg = 'Failed: you were hit by a car!'
        restart = True
    elif msg == 'won':
        msg = 'Succeeded: you have crossed the street safely!'
        restart = True
    
    score_pen.clear()
    score_pen.write(msg, align = "center", font=("Verdana", 18, "normal"))

    if (restart):
        frog.hideturtle()
        frog.goto(0, Lane.lowest_lane_y-50)
        frog.showturtle()
        time.sleep(2)
        score_pen.clear()

while True:
    for l in lanes:
        l.flow()

screen.mainloop()
            