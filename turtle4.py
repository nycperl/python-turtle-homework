import turtle, random

"""
description:
bouncing balls with collision detection
"""

PLAYGROUND_SIDE =200
COLLISION_BUFFER = 12

class Ball(turtle.Turtle):
    step = 5
   
    def __init__(self, color):
        super().__init__()
        self.outside_border = False
        self.ending_ycor = None
        #self.resizemode('user')
        self.shape('circle')
        #self.shapesize(1,1)
        self.color(color)
        self.goto(0, 0)
        self.name = color
        #self.mode('standard')
        init_heading = random.randrange(0, 360)
        self.setheading(init_heading)
        self.penup()
        

    def move(self):
        self.forward(Ball.step)
        #print (self.name, self.xcor(), self.ycor())
        # if hitting the border, then change dir
        if self.xcor() >= PLAYGROUND_SIDE or self.xcor() <= (-1) * PLAYGROUND_SIDE:
            self.setheading( 180 - self.heading() )
        if self.ycor() >= PLAYGROUND_SIDE or self.ycor() <= (-1) * PLAYGROUND_SIDE:
            self.setheading( 360 - self.heading() )

class Game():
    def __init__(self, b1, b2, b3):
        self.b1 = b1
        self.b2 = b2
        self.b3 = b3 

    def play(self):
        count = 0
        while True:
            count += 1
            for _ in (self.b1, self.b2, self.b3):
                _.move()
            # delayed collision detetion from original starting point
            if count > 20: self.detect_collision()

    def detect_collision(self):
        if abs(self.b1.xcor() - self.b2.xcor()) < COLLISION_BUFFER and abs(self.b1.ycor() - self.b2.ycor()) < COLLISION_BUFFER:
            print (self.b1.name, ' hit ', self.b2.name, ' at ', int(self.b1.xcor()), int(self.b1.ycor()))
            self.write_text(self.b1)
            self.write_text(self.b2)
        if abs(self.b1.xcor() - self.b3.xcor()) < COLLISION_BUFFER and abs(self.b1.ycor() - self.b3.ycor()) < COLLISION_BUFFER:
            print (self.b1.name, ' hit ', self.b3.name, ' at ', int(self.b1.xcor()), int(self.b1.ycor()))
            self.write_text(self.b1)
            self.write_text(self.b3)
        if abs(self.b3.xcor() - self.b2.xcor()) < COLLISION_BUFFER and abs(self.b3.ycor() - self.b2.ycor()) < COLLISION_BUFFER:
            print (self.b3.name, ' hit ', self.b2.name, ' at ', int(self.b3.xcor()), int(self.b3.ycor()))
            self.write_text(self.b2)
            self.write_text(self.b3)
     
    def write_text(self, b):
        hit_text = "+"
        b.write(hit_text, move = False, align = "right", font = ("Arial", 14, "bold"))

def draw_playground():
    turtle.color('black')
    turtle.shape('circle')
    turtle.penup()
    # spare coord: (-200, 200), (200, 200), (200, -200), (-200, 200)
    turtle.goto(PLAYGROUND_SIDE * (-1), PLAYGROUND_SIDE)
    turtle.pendown()
    for _ in range(4):
        turtle.forward(PLAYGROUND_SIDE * 2)
        turtle.stamp()
        turtle.right(90)

draw_playground()

ball1 = Ball('red')
ball2 = Ball('blue')
ball3 = Ball('green')

game = Game(ball1, ball2, ball3)
game.play()