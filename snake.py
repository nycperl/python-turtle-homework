import random
import time
import turtle 

"""
description:
greedy snake game
"""

# Init variables
height = 8
width = 8
grid = []
eat_count = 5
snake = []
food = set([])
food_percentage = 0.2
steps = 0
origin = ()

WALL_COLOR = 'white'
SNAKE_COLOR = 'red'
FOOD_COLOR = 'blue'
WALL_POS_CONV = 22
WALL_POS_OFFSET_X = width * WALL_POS_CONV / 2
WALL_POS_OFFSET_Y = height * WALL_POS_CONV / 2

 
class Wall(turtle.Turtle):
    
    def __init__(self, x, y):
        super().__init__()
        self.hideturtle()
        self.shape("square")
        self.color(WALL_COLOR) 
        self.penup()
        self.shapesize(stretch_wid=1, stretch_len=1)
        x = convert_x(x)
        y = convert_y(y)
        #print (x, y)
        self.goto(x, y)
        self.showturtle()
        self.name = (x,y)

class Player(turtle.Turtle):
    def __init__(self, x, y):
        super().__init__()
        self.hideturtle()
        self.shape("turtle")
        self.color(SNAKE_COLOR)
        self.penup()
        x = convert_x(x)
        y = convert_y(y)
        #print (x, y)
        self.goto(x, y)
        self.showturtle()

def convert_x(x):
    return x * WALL_POS_CONV  - WALL_POS_OFFSET_X

def convert_y(y):
    return y * WALL_POS_CONV  - WALL_POS_OFFSET_Y

def restore_x(x):
    return (x + WALL_POS_OFFSET_X)/WALL_POS_CONV

def restore_y(y):
    return (y + WALL_POS_OFFSET_Y)/WALL_POS_CONV

def can_move(direction):
    # current index in the 2-D array
    x = int(restore_x(player.xcor()))
    y = int(restore_y(player.ycor()))
    #print("x=",x, "y=",y)

    if (direction == 'up' and (y >= height-1 or grid[y+1][x].color()[0] == SNAKE_COLOR )):
        return False
    if (direction == 'down' and (y <= 0 or grid[y-1][x].color()[0] == SNAKE_COLOR )):
        return False
    if (direction == 'left' and (x <= 0 or grid[y][x-1].color()[0] == SNAKE_COLOR )):
        return False
    if (direction == 'right' and (x >= width-1 or grid[y][x+1].color()[0] == SNAKE_COLOR )):
        return False 
    
    global eat_count
    if (x,y) in food:
        eat_count += 1
        food.remove((x,y))
    
    snake.insert(0, grid[y][x])
    #print ('food len:', len(food), ' eat:', eat_count, ' snake:', len(snake))
    #print ('food', food)
    for c in range(min(len(food)+eat_count+1, len(snake))-1, -1, -1):
        if c < eat_count: 
            snake[c].color(SNAKE_COLOR)
        else:
            snake[c].color(WALL_COLOR)
        #print (snake[c].name)
    
    global steps
    steps += 1
    msg = 'Food Eaten/Remaining: '+str(eat_count)+'/'+str(len(food))
    if not len(food):
        msg += "  -- You Won!!"
    update_score(msg)
    screen.update()
    return True

def update_score(s):
    pen.clear()
    pen.write(s, font=("Arial", 25))

def move_left():
    if not can_move('left'):
        return
    player.setheading(180)
    player.setx(convert_x(restore_x(player.xcor())-1))
    
def move_right():
    if not can_move('right'):
        return
    player.setheading(0)
    player.setx(convert_x(restore_x(player.xcor())+1))

def move_up():
    if not can_move('up'):
        return
    player.setheading(90)
    player.sety(convert_y(restore_y(player.ycor())+1))

def move_down():
    if not can_move('down'):
        return
    player.setheading(270)
    player.sety(convert_y(restore_y(player.ycor())-1))


def populate_grid_array():
    # Denote all cells as unvisited
    for i in range(0, height):
        line = []
        for j in range(0, width):
            line.append(Wall(j, i))
        grid.append(line)

    def random_num(i):
        return int(random.random()*i)

    # Randomize starting point and set it visited 
    starting_height = random_num(height)
    starting_width = random_num(width)
    if (starting_height == 0):
        starting_height += 1
    if (starting_height == height-1):
        starting_height -= 1
    if (starting_width == 0):
        starting_width += 1
    if (starting_width == width-1):
        starting_width -= 1

    #grid[starting_width][starting_height].color(SNAKE_COLOR)
    global origin
    origin = (starting_width, starting_height)

    while len(food) <= int(height*width*food_percentage): 
        pair = (random_num(width), random_num(height))
        if pair == origin:
            continue
        food.add(pair)
        grid[pair[1]][pair[0]].color(FOOD_COLOR)




screen = turtle.Screen()
screen.bgcolor("orange")
screen.title("Snake Game")
populate_grid_array()

player = Player(origin[0], origin[1])
screen.listen()
screen.onkey(move_left, 'Left')
screen.onkey(move_right, 'Right')
screen.onkey(move_up, 'Up')
screen.onkey(move_down, 'Down')

pen = turtle.Turtle()
pen.hideturtle()
pen.color("white")
pen.penup()
pen.goto(-300, 300)



screen.mainloop()
